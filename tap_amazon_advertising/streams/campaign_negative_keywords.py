from tap_amazon_advertising.streams.base import PaginatedStream

import singer
import json

LOGGER = singer.get_logger()  # noqa


class CampaignNegativeKeywordsStream(PaginatedStream):
    API_METHOD = 'POST'
    TABLE = 'campaign_negative_keywords'
    KEY_PROPERTIES = ['profileId', 'keywordId']
    CONTENT_TYPE = "application/vnd.spcampaignNegativeKeyword.v3+json"

    @property
    def api_path(self):
        return '/sp/campaignNegativeKeywords/list'

    def get_stream_data(self, result):
        return [
            self.transform_record(record)
            for record in result.get("campaignNegativeKeywords", [])
        ]
