from tap_amazon_advertising.streams.base import PaginatedStream

import singer
import json

LOGGER = singer.get_logger()  # noqa


class AdGroupsStream(PaginatedStream):
    API_METHOD = 'POST'
    TABLE = 'ad_groups'
    KEY_PROPERTIES = ['profileId', 'adGroupId']
    CONTENT_TYPE = "application/vnd.spadGroup.v3+json"

    @property
    def api_path(self):
        return '/sp/adGroups/list'

    def get_stream_data(self, result):
        return [
            self.transform_record(record)
            for record in result.get("adGroups", [])
        ]
