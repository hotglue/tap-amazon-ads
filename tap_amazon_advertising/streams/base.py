import math
import pytz
import singer
import singer.utils
import singer.metrics
import time
import datetime

from tap_amazon_advertising.client import AmazonAdvertisingClient
from tap_amazon_advertising.config import get_config_start_date
from tap_amazon_advertising.state import incorporate, save_state, \
    get_last_record_value_for_table

from tap_framework.streams import BaseStream as base

LOGGER = singer.get_logger()

DEFAULT_BASE_URL = 'https://advertising-api.amazon.com'
AVAILABLE_BASE_URL = [
    "https://advertising-api.amazon.com",
    "https://advertising-api-eu.amazon.com",
    "https://advertising-api-fe.amazon.com",
]

class BaseStream(base):
    KEY_PROPERTIES = ['id']
    CONTENT_TYPE = None

    def get_params(self):
        return {}

    def get_body(self):
        return {}

    def get_url(self, path):
        api_base_url = self.config.get("api_base_url")
        if api_base_url is None:
            uri = self.config.get("uri")

            if uri is None:
                api_base_url = DEFAULT_BASE_URL
            else:
                if uri == "https://www.amazon.com":
                    api_base_url = "https://advertising-api.amazon.com"
                elif uri == "https://eu.account.amazon.com":
                    api_base_url = "https://advertising-api-eu.amazon.com"
                elif uri == "https://apac.account.amazon.com":
                    api_base_url = "https://advertising-api-fe.amazon.com"

        if api_base_url not in AVAILABLE_BASE_URL:
            raise Exception(f"The `api_base_url` config should be on of these:{AVAILABLE_BASE_URL}. Value used: {api_base_url}")

        return '{}{}'.format(api_base_url, path)

    def transform_record(self, record, inject_profile=True):
        if inject_profile:
            record['profileId'] = str(self.config.get('profile_id'))
        transformed = base.transform_record(self, record)

        return transformed

    def sync_data(self):
        table = self.TABLE
        LOGGER.info('Syncing data for entity {}'.format(table))

        url = self.get_url(self.api_path)
        params = self.get_params()
        body = self.get_body()
        headers = None
        if self.CONTENT_TYPE:
            headers = {'Accept': self.CONTENT_TYPE, 'Content-Type': self.CONTENT_TYPE}

        client: AmazonAdvertisingClient = self.client

        result = client.make_request_json(
            url, self.API_METHOD, params=params, body=body, headers=headers)
        data = self.get_stream_data(result)

        with singer.metrics.record_counter(endpoint=table) as counter:
            for obj in data:
                singer.write_records(
                    table,
                    [obj])

                counter.increment()
        return self.state

class PaginatedStream(BaseStream):

    def sync_data(self):
        table = self.TABLE
        LOGGER.info('Syncing data for entity {}'.format(table))

        url = self.get_url(self.api_path)
        body = self.get_body()
        client: AmazonAdvertisingClient = self.client

        page_count = 0
        while True:
            LOGGER.info('Syncing from page {}'.format(page_count))
            headers = {'Accept': self.CONTENT_TYPE, 'Content-Type': self.CONTENT_TYPE} if self.CONTENT_TYPE else {}
            result = client.make_request(
                url, self.API_METHOD, body=body, headers=headers)
            data = self.get_stream_data(result.json())
            with singer.metrics.record_counter(endpoint=table) as counter:
                for obj in data:
                    singer.write_records(
                        table,
                        [obj])
                    counter.increment()
            if not hasattr(result, 'next') or result.next is None:
                break
            else:
                url = result.next
                page_count += 1
        return self.state


class ReportStream(BaseStream):
    def create_report(self, url, day):
        body = self.get_body(day)

        # Create a report
        report = self.client.make_request(url, 'POST', body=body)
        report_id = report['reportId']

        # If we don't sleep here, then something funky happens and the API
        # takes _significantly_ longer to return a SUCCESS status
        time.sleep(10)
        LOGGER.info("Polling")
        report_url = '{}/v2/reports/{}'.format(BASE_URL, report_id)

        num_polls = 7
        for i in range(num_polls):
            poll = self.client.make_request(report_url, 'GET')
            status = poll['status']
            LOGGER.info("Poll {} of {}, status={}".format(i+1, num_polls, status))

            if status == 'SUCCESS':
                return poll['location']
            else:
                timeout = (1 + i) ** 2
                LOGGER.info("In state: {}, Sleeping for {} seconds".format(status, timeout))
                time.sleep(timeout)

        LOGGER.info("Unable to sync from {} for day {}-- moving on".format(url, day))

    def sync_data(self):
        table = self.TABLE
        LOGGER.info('Syncing data for entity {}'.format(table))

        yesterday = datetime.date.today() - datetime.timedelta(days=1)

        sync_date = get_last_record_value_for_table(self.state, table)
        if sync_date is None:
            sync_date = get_config_start_date(self.config)

        # Add a lookback to refresh attribution metrics for more recent orders
        sync_date -= datetime.timedelta(days=self.config.get('lookback', 30))

        while sync_date <= yesterday:
            LOGGER.info("Syncing {} for date {}".format(table, sync_date))

            url = self.get_url(self.api_path)
            report_url = self.create_report(url, sync_date)

            if report_url is None:
                break

            result = self.client.download_gzip(report_url)
            data = self.get_stream_data(result, sync_date)

            with singer.metrics.record_counter(endpoint=table) as counter:
                for obj in data:
                    singer.write_records(
                        table,
                        [obj])

                    counter.increment()


            self.state = incorporate(self.state, self.TABLE,
                                     'last_record', sync_date.isoformat())
            save_state(self.state)

            sync_date += datetime.timedelta(days=1)

        return self.state
