from tap_amazon_advertising.streams.base import PaginatedStream

import singer
import json

LOGGER = singer.get_logger()  # noqa


class ProductAdsStream(PaginatedStream):
    API_METHOD = 'POST'
    TABLE = 'product_ads'
    KEY_PROPERTIES = ['profileId', 'adId']
    CONTENT_TYPE = "application/vnd.spproductAd.v3+json"

    @property
    def api_path(self):
        return '/sp/productAds/list'

    def get_stream_data(self, result):
        return [
            self.transform_record(record)
            for record in result.get("productAds", [])
        ]
