from tap_amazon_advertising.streams.base import PaginatedStream

import singer
import json

LOGGER = singer.get_logger()  # noqa


class SponsoredProductsCampaignsStream(PaginatedStream):
    API_METHOD = 'POST'
    TABLE = 'sponsored_products_campaigns'
    KEY_PROPERTIES = ['campaignId', 'profileId']
    CONTENT_TYPE = 'application/vnd.spcampaign.v3+json'

    @property
    def api_path(self):
        return '/sp/campaigns/list'

    def get_stream_data(self, result):
        return [
            self.transform_record(record)
            for record in result.get("campaigns", [])
        ]
