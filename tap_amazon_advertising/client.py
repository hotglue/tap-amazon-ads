import requests
import requests_oauthlib
import singer
import singer.metrics

import zlib
import json
import time

LOGGER = singer.get_logger()  # noqa

TOKEN_URL = 'https://api.amazon.com/auth/o2/token'
SCOPES = [
    # "cpc_advertising:campaign_management", 
    "advertising::campaign_management",
    # "advertising::test:create_account"
]


class AmazonAdvertisingClient:

    MAX_TRIES = 12

    def __init__(self, config):
        self.config = config
        self.access_token = self.get_authorization()

    def get_authorization(self):
        client_id = self.config.get('client_id')
        oauth = requests_oauthlib.OAuth2Session(
                    client_id,
                    redirect_uri=self.config.get('redirect_uri'),
                    scope=SCOPES)

        tokens = oauth.refresh_token(
                    TOKEN_URL,
                    refresh_token=self.config.get('refresh_token'),
                    client_id=self.config.get('client_id'),
                    client_secret=self.config.get('client_secret'))

        LOGGER.info(f"Got tokens: {tokens}")
        return tokens['access_token']

    def make_request(self, url, method, params=None, body=None, headers=None, attempts=0):
        LOGGER.info("Making {} request to {} ({})".format(method, url, params))
        if headers is None:
            headers = {
                'Content-Type': 'application/json'
            }

        headers['Authorization'] = 'Bearer {}'.format(self.access_token)
        headers['Amazon-Advertising-API-ClientId'] = self.config.get('client_id')
        headers['Amazon-Advertising-API-Scope'] = str(self.config.get('profile_id'))

        if url.endswith("/profiles"):
            headers.pop("Amazon-Advertising-API-Scope", None)

        if params and len(body)>0:
            response = requests.request(
                method,
                url,
                headers=headers,
                params=params,
                json=body)
        elif params is not None and not body:
            response = requests.request(
                method,
                url,
                headers=headers,
                params=params)        
        elif body is not None and not params:
            response = requests.request(
                method,
                url,
                headers=headers,
                json=body)        
        else:
             response = requests.request(
                method,
                url,
                headers=headers)

        message = f"[Status Code: {response.status_code}] Response: {response.text}"
        LOGGER.info(message)

        if attempts < self.MAX_TRIES and response.status_code not in [200, 201, 202]:
            if response.status_code == 401:
                LOGGER.info(f"[Status Code: {response.status_code}] Attempt {attempts} of {self.MAX_TRIES}: Received unauthorized error code, retrying: {response.text}")
                self.access_token = self.get_authorization()
            else:
                sleep_duration = 2 ** attempts
                message = f"[Status Code: {response.status_code}] Attempt {attempts} of {self.MAX_TRIES}: Error: {response.text}, Sleeping: {sleep_duration} seconds"
                LOGGER.warning(message)
                time.sleep(sleep_duration)

            return self.make_request(url, method, params, body, headers, attempts+1)

        if response.status_code not in [200, 201, 202]:
            message = f"[Status Code: {response.status_code}] Error {response.text} for url {response.url}"
            LOGGER.error(message)
            raise RuntimeError(message)

        return response

    def make_request_json(self, url, method, params=None, body=None, headers=None):
        return self.make_request(url, method, params, body, headers).json()

    def download_gzip(self, url):
        resp = None
        attempts = 3
        for i in range(attempts + 1):
            try:
                resp = self.make_request(url, 'GET')
                break
            except ConnectionError as e:
                LOGGER.info("Caught error while downloading gzip, sleeping: {}".format(e))
                time.sleep(10)
        else:
            raise RuntimeError("Unable to sync gzip after {} attempts".format(attempts))

        return self.unzip(resp.content)

    @classmethod
    def unzip(cls, blob):
        extracted = zlib.decompress(blob, 16+zlib.MAX_WBITS)
        decoded = extracted.decode('utf-8')
        return json.loads(decoded)
